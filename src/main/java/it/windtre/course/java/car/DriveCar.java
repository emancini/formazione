package it.windtre.course.java.car;

public class DriveCar {

    public static void main(String[] args){
        Car mauriziosCar = new Car();
        mauriziosCar.startEngine();
        mauriziosCar.goStraight();
        mauriziosCar.turnLeft();
        mauriziosCar.turnRight();
        mauriziosCar.stopEngine();
    }
}
