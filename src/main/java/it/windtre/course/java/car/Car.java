package it.windtre.course.java.car;

public class Car {

    private boolean engine;
    private boolean light;
    private boolean arrowLeft;
    private boolean arrowRight;

    public Car() {
        this.engine = false;
        this.light = false;
        this.arrowLeft = false;
        this.arrowRight = false;
    }

    public void startEngine(){
        System.out.println("Start Engine");
        this.engine=true;
        this.light=true;
    }

    public void turnLeft(){
        System.out.println("Blink Left");
        this.arrowLeft=true;
    }

    public void turnRight(){
        System.out.println("Blink Right");
        this.arrowRight=true;
    }

    public void goStraight(){
        this.arrowRight=false;
        this.arrowLeft=false;
        System.out.println("Wrroooooom");
    }

    public void stopEngine(){
        this.engine=false;
        this.light=false;
        System.out.println("Bye Bye");
    }

    public boolean isEngine() {
        return engine;
    }

    public void setEngine(boolean engine) {
        this.engine = engine;
    }

    public boolean isLight() {
        return light;
    }

    public void setLight(boolean light) {
        this.light = light;
    }

    public boolean isArrowLeft() {
        return arrowLeft;
    }

    public void setArrowLeft(boolean arrowLeft) {
        this.arrowLeft = arrowLeft;
    }

    public boolean isArrowRight() {
        return arrowRight;
    }

    public void setArrowRight(boolean arrowRight) {
        this.arrowRight = arrowRight;
    }
}
