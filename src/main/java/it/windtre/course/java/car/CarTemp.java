package it.windtre.course.java.car;

public class CarTemp {

    private boolean engine;
    private boolean light;
    private boolean arrowLeft;
    private boolean arrowRight;



    public CarTemp() {
        this.engine = false;
        this.light = false;
        this.arrowLeft = false;
        this.arrowRight = false;
    }

    public void engine() throws InterruptedException {
        System.out.println("Start Engine...");
        Thread.sleep(1000);
        System.out.println("Check Engine...");
        Thread.sleep(1000);
        System.out.println("bruuuuum...");
        this.engine = true;
        Thread.sleep(1000);
        System.out.println("Motore Avviato correttamente...");
        Thread.sleep(1000);
        System.out.println("Accensioni Luce...");
        this.light = true;
    }

    public void turnLeft() {
        this.arrowLeft = true;
        printAction("Blink left...");
    }

    public void turnRigth() {
        this.arrowRight = true;
        printAction("Blink right...");
    }

    public void goAway() {
        this.arrowLeft = false;
        this.arrowRight = false;
        printAction("Wroooom...");

    }

    public void engineOff() throws InterruptedException {
        System.out.println("bruuuuum...");
        Thread.sleep(1000);
        System.out.println("Spengimento Luci...");
        this.light = false;
        this.arrowLeft=false;
        this.arrowRight=false;
        Thread.sleep(1000);
        System.out.println("Check Engine...");
        Thread.sleep(1000);
        System.out.println("Stop Engine...");
        this.engine = false;
        Thread.sleep(1000);
        System.out.println("Bye Bye");
    }

    private void printAction(String s){
        int c = 0;
        while (c < 5) {
            c++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(s);
        }
    }




    public boolean isEngine() {
        return engine;
    }

    public void setEngine(boolean engine) {
        this.engine = engine;
    }

    public boolean isLight() {
        return light;
    }

    public void setLight(boolean light) {
        this.light = light;
    }

    public boolean isArrowLeft() {
        return arrowLeft;
    }

    public void setArrowLeft(boolean arrowLeft) {
        this.arrowLeft = arrowLeft;
    }

    public boolean isArrowRight() {
        return arrowRight;
    }

    public void setArrowRight(boolean arrowRight) {
        this.arrowRight = arrowRight;
    }
}
