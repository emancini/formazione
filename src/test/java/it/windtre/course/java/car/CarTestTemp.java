package it.windtre.course.java.car;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CarTestTemp {

    private CarTemp car = new CarTemp();



    @Test
    @Order(1)
    public void givenCarWhenEngineThenStartCar() throws InterruptedException {
        assertFalse(car.isEngine());
        assertFalse(car.isLight());
        car.engine();
        assertTrue(car.isEngine());
        assertTrue(car.isLight());
    }

    @Test
    @Order(2)
    public void givenEngineCarWhenTurnLeftThenArrowLeft() throws InterruptedException {
        car.turnLeft();
        assertTrue(car.isArrowLeft());
    }

    @Test
    @Order(3)
    public void givenEngineCarWhenGoAwayThenOffArrows() throws InterruptedException {
        Thread.sleep(2000);
        car.goAway();
        assertFalse(car.isArrowLeft());
        assertFalse(car.isArrowRight());
    }

    @Test
    @Order(2)
    public void givenEngineCarWhenTurnRightThenArrowRight() throws InterruptedException {
        car.turnRigth();
        assertTrue(car.isArrowLeft());
    }

}